//
//  SpaceshipScene.m
//  SpriteWalkthrough
//
//  Created by Vitalii Krayovyi on 8/19/13.
//  Copyright (c) 2013 Master of Code. All rights reserved.
//

#import "SpaceshipScene.h"

static const uint32_t asteroidCategory      =  0x1 << 0;
static const uint32_t shipCategory          =  0x1 << 1;

@interface SpaceshipScene ()

@property BOOL contentCreated;

@end

@implementation SpaceshipScene

-(id)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self)
    {
        self.physicsWorld.contactDelegate = self;
    }
    return self;
}

#pragma mark - SKContactDelegate
-(void)didBeginContact:(SKPhysicsContact *)contact
{
    SKAction *playSound = [SKAction playSoundFileNamed:@"Punch.mp3" waitForCompletion:NO];
    SKPhysicsBody *firstBody, *secondBody;
    
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    else
    {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    if ((firstBody.categoryBitMask & secondBody.categoryBitMask) == 0)
    {
        [self runAction:playSound];
    }
}

-(void)didEndContact:(SKPhysicsContact *)contact
{

}

- (void)didMoveToView:(SKView *)view
{
    if (!self.contentCreated)
    {
        [self createSceneContents];
        self.contentCreated = YES;
    }
}

- (void)createSceneContents
{
    self.backgroundColor = [SKColor blackColor];
    self.scaleMode = SKSceneScaleModeAspectFit;
    
    SKSpriteNode *spaceship = [self newSpaceship];
    spaceship.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)-150);
    [self addChild:spaceship];
    
    SKAction *makeRocks = [SKAction sequence: @[
                                                [SKAction performSelector:@selector(addRock) onTarget:self],
                                                [SKAction waitForDuration:0.10 withRange:0.15]
                                                ]];
    [self runAction: [SKAction repeatActionForever:makeRocks]];
}

- (SKSpriteNode *)newSpaceship
{
    SKSpriteNode *hull = [[SKSpriteNode alloc] initWithColor:[SKColor grayColor] size:CGSizeMake(64,32)];
    hull.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:hull.size];
    hull.physicsBody.dynamic = NO;
    hull.physicsBody.categoryBitMask = shipCategory;
    hull.physicsBody.contactTestBitMask = shipCategory | asteroidCategory;
    
    SKSpriteNode *light1 = [self newLight];
    light1.position = CGPointMake(-28.0, 6.0);
    [hull addChild:light1];
    
    SKSpriteNode *light2 = [self newLight];
    light2.position = CGPointMake(28.0, 6.0);
    [hull addChild:light2];
    
    SKAction *hover = [SKAction sequence:@[
                                           [SKAction waitForDuration:1.0],
                                           [SKAction moveByX:100 y:50.0 duration:1.0],
                                           [SKAction waitForDuration:1.0],
                                           [SKAction moveByX:-100.0 y:-50 duration:1.0]]];
    [hull runAction: [SKAction repeatActionForever:hover]];
    
    return hull;
}

- (SKSpriteNode *)newLight
{
    SKSpriteNode *light = [[SKSpriteNode alloc] initWithColor:[SKColor yellowColor] size:CGSizeMake(8,8)];
    
    SKAction *blink = [SKAction sequence:@[
                                           [SKAction fadeOutWithDuration:0.25],
                                           [SKAction fadeInWithDuration:0.25]]];
    SKAction *blinkForever = [SKAction repeatActionForever:blink];
    [light runAction: blinkForever];
    
    return light;
}

static inline CGFloat skRandf() {
    return rand() / (CGFloat) RAND_MAX;
}

static inline CGFloat skRand(CGFloat low, CGFloat high) {
    return skRandf() * (high - low) + low;
}

- (void)addRock
{
    SKTexture *rockTexture = [SKTexture textureWithImageNamed:@"asteroid_1.png"];

    SKSpriteNode *rock = [SKSpriteNode spriteNodeWithTexture:rockTexture];
    rock.size = CGSizeMake(56., 57.);
    rock.position = CGPointMake(skRand(0, self.size.width), self.size.height-50);
    rock.name = @"rock";
    rock.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:rock.size];
    rock.physicsBody.usesPreciseCollisionDetection = YES;
    rock.physicsBody.categoryBitMask = asteroidCategory;
    rock.physicsBody.contactTestBitMask = shipCategory | asteroidCategory;
    [self addChild:rock];
}

-(void)didSimulatePhysics
{
    [self enumerateChildNodesWithName:@"rock" usingBlock:^(SKNode *node, BOOL *stop) {
        if (node.position.y < 0)
            [node removeFromParent];
    }];
}

@end
