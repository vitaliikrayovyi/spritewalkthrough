//
//  SpriteViewController.h
//  SpriteWalkthrough
//
//  Created by Vitalii Krayovyi on 8/19/13.
//  Copyright (c) 2013 Master of Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface SpriteViewController : UIViewController

@end
