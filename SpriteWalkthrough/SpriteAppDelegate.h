//
//  SpriteAppDelegate.h
//  SpriteWalkthrough
//
//  Created by Vitalii Krayovyi on 8/19/13.
//  Copyright (c) 2013 Master of Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpriteAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
